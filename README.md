# Rent My Ride

This is the assessment task for Rent My Ride Position. 

## Summary

This project uses Laravel as a back-end server and React for the front-end.
There are two folders 'Server' and 'Client'. The Server folder contains the Laravel source code and the Client folder contains the React source code

## How to run

1. Run the whole project as a Laravel Project using Homestead.
2. Open Terminal and run `sudo nano /etc/hosts` and add `<ip> rmr.local` where <ip> is the IP address of your Homestead found in the Homestead.yaml file.
3. In your Terminal navigate to the Homestead folder and run `vagrant provision` 
4. In your Terminal navigate to the Projects's Client folder and run `npm install` then `npm start`

The project will now be running on [localhost:3000](http://localhost:3000/)